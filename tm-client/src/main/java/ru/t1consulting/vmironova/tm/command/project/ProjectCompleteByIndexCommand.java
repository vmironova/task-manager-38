package ru.t1consulting.vmironova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.dto.request.ProjectCompleteByIndexRequest;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Complete project by id.";

    @NotNull
    public static final String NAME = "project-complete-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(getToken());
        request.setIndex(index);
        getProjectEndpoint().completeByIndexProject(request);
    }

}
