package ru.t1consulting.vmironova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.vmironova.tm.api.repository.IUserRepository;
import ru.t1consulting.vmironova.tm.api.service.IConnectionService;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;
import ru.t1consulting.vmironova.tm.api.service.ISessionService;
import ru.t1consulting.vmironova.tm.exception.field.IdEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.vmironova.tm.marker.UnitCategory;
import ru.t1consulting.vmironova.tm.model.Session;
import ru.t1consulting.vmironova.tm.model.User;
import ru.t1consulting.vmironova.tm.repository.UserRepository;

import java.sql.Connection;
import java.util.List;

import static ru.t1consulting.vmironova.tm.constant.SessionTestData.*;
import static ru.t1consulting.vmironova.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1consulting.vmironova.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);
    @NotNull
    private static String userId = "";
    @NotNull
    private final ISessionService service = new SessionService(connectionService);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = userRepository.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = userRepository.findByLogin(USER_TEST_LOGIN);
        if (user != null) userRepository.remove(user);
    }

    @Before
    public void before() throws Exception {
        service.add(userId, USER_SESSION1);
        service.add(userId, USER_SESSION2);
    }

    @After
    public void after() throws Exception {
        service.clear(userId);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, USER_SESSION3);
        });
        Assert.assertNotNull(service.add(userId, USER_SESSION3));
        @Nullable final Session session = service.findOneById(userId, USER_SESSION3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION3.getId(), session.getId());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        final List<Session> sessions = service.findAll(userId);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(userId, session.getUserId()));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_SESSION_ID);
        });
        Assert.assertFalse(service.existsById(userId, ""));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(userId, USER_SESSION1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(userId, "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_SESSION1.getId());
        });
        Assert.assertNull(service.findOneById(userId, NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(userId, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        service.clear(userId);
        Assert.assertEquals(0, service.getSize(userId));
    }

    @Test
    public void remove() throws Exception {
        @Nullable final Session removedSession = service.remove(USER_SESSION2);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(USER_SESSION2, removedSession);
        Assert.assertNull(service.findOneById(USER_SESSION2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        @Nullable final Session removedSession = service.remove(userId, USER_SESSION2);
        Assert.assertEquals(USER_SESSION2.getId(), removedSession.getId());
        Assert.assertNull(service.findOneById(userId, USER_SESSION2.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(userId, null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(userId, "");
        });
        Assert.assertNull(service.removeById(userId, NON_EXISTING_SESSION_ID));
        @Nullable final Session removedSession = service.removeById(userId, USER_SESSION2.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(USER_SESSION2.getId(), removedSession.getId());
        Assert.assertNull(service.findOneById(userId, USER_SESSION2.getId()));
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize("");
        });
        Assert.assertEquals(2, service.getSize(userId));
    }

}
