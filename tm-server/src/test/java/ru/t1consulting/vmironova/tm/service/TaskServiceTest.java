package ru.t1consulting.vmironova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.vmironova.tm.api.repository.IUserRepository;
import ru.t1consulting.vmironova.tm.api.service.IConnectionService;
import ru.t1consulting.vmironova.tm.api.service.IProjectService;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;
import ru.t1consulting.vmironova.tm.api.service.ITaskService;
import ru.t1consulting.vmironova.tm.comparator.NameComparator;
import ru.t1consulting.vmironova.tm.enumerated.Sort;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.vmironova.tm.exception.field.*;
import ru.t1consulting.vmironova.tm.marker.UnitCategory;
import ru.t1consulting.vmironova.tm.model.Task;
import ru.t1consulting.vmironova.tm.model.User;
import ru.t1consulting.vmironova.tm.repository.UserRepository;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.t1consulting.vmironova.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1consulting.vmironova.tm.constant.ProjectTestData.USER_PROJECT2;
import static ru.t1consulting.vmironova.tm.constant.TaskTestData.*;
import static ru.t1consulting.vmironova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);
    @NotNull
    private static String userId = "";
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);
    @NotNull
    private final ITaskService service = new TaskService(connectionService);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = userRepository.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = userRepository.findByLogin(USER_TEST_LOGIN);
        if (user != null) userRepository.remove(user);
    }

    @Before
    public void before() throws Exception {
        projectService.add(userId, USER_PROJECT1);
        projectService.add(userId, USER_PROJECT2);
        service.add(userId, USER_TASK1);
        service.add(userId, USER_TASK2);
    }

    @After
    public void after() throws Exception {
        service.clear(userId);
        projectService.clear(userId);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add("", USER_TASK3);
        });
        Assert.assertNotNull(service.add(userId, USER_TASK3));
        @Nullable final Task task = service.findOneById(userId, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3.getId(), task.getId());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        final List<Task> tasks = service.findAll(userId);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @Nullable Comparator comparator = null;
        Assert.assertNotNull(service.findAll(userId, comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            service.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        final List<Task> tasks = service.findAll(userId, comparator);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllSortByUserId() throws Exception {
        @Nullable Sort sort = null;
        Assert.assertNotNull(service.findAll(userId, sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            service.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        final List<Task> tasks = service.findAll(userId, sort);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_TASK_ID);
        });
        Assert.assertFalse(service.existsById(userId, ""));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(userId, USER_TASK1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(userId, "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_TASK1.getId());
        });
        Assert.assertNull(service.findOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        service.clear(userId);
        Assert.assertEquals(0, service.getSize(userId));
    }

    @Test
    public void remove() throws Exception {
        @Nullable final Task removedTask = service.remove(USER_TASK2);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(USER_TASK2, removedTask);
        Assert.assertNull(service.findOneById(USER_TASK2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        @Nullable final Task removedTask = service.remove(userId, USER_TASK2);
        Assert.assertEquals(USER_TASK2.getId(), removedTask.getId());
        Assert.assertNull(service.findOneById(userId, USER_TASK2.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(userId, null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(userId, "");
        });
        Assert.assertNull(service.removeById(userId, NON_EXISTING_TASK_ID));
        @Nullable final Task removedTask = service.removeById(userId, USER_TASK2.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(USER_TASK2.getId(), removedTask.getId());
        Assert.assertNull(service.findOneById(userId, USER_TASK2.getId()));
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize("");
        });
        Assert.assertEquals(2, service.getSize(userId));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_TASK3.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", USER_TASK3.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, "");
        });
        @NotNull final Task task = service.create(userId, USER_TASK3.getName());
        Assert.assertEquals(task.getId(), service.findOneById(userId, task.getId()).getId());
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_TASK3.getName(), USER_TASK3.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", USER_TASK3.getName(), USER_TASK3.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, null, USER_TASK3.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, "", USER_TASK3.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(userId, USER_TASK3.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(userId, USER_TASK3.getName(), "");
        });
        @NotNull final Task task = service.create(userId, USER_TASK3.getName(), USER_TASK3.getDescription());
        Assert.assertEquals(task.getId(), service.findOneById(userId, task.getId()).getId());
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(USER_TASK3.getDescription(), task.getDescription());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(userId, null, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(userId, "", USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(userId, USER_TASK1.getId(), null, USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(userId, USER_TASK1.getId(), "", USER_TASK1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.updateById(userId, USER_TASK1.getId(), USER_TASK1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.updateById(userId, USER_TASK1.getId(), USER_TASK1.getName(), "");
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.updateById(userId, NON_EXISTING_TASK_ID, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        @NotNull final String name = USER_TASK1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = USER_TASK1.getDescription() + NON_EXISTING_TASK_ID;
        service.updateById(userId, USER_TASK1.getId(), name, description);
        @Nullable final Task task = service.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void changeTaskStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById(null, USER_TASK1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById("", USER_TASK1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(userId, null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(userId, "", status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeTaskStatusById(userId, USER_TASK1.getId(), null);
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.changeTaskStatusById(userId, NON_EXISTING_TASK_ID, status);
        });
        service.changeTaskStatusById(userId, USER_TASK1.getId(), status);
        @Nullable final Task task = service.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void findAllByProjectId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @NotNull final Collection<Task> testCollection = service.findAllByProjectId(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @NotNull final Collection<Task> testCollection = service.findAllByProjectId("", USER_PROJECT1.getId());
        });
        @NotNull final Collection<Task> emptyCollection = Collections.emptyList();
        Assert.assertEquals(emptyCollection, service.findAllByProjectId(USER_TEST.getId(), null));
        Assert.assertEquals(emptyCollection, service.findAllByProjectId(USER_TEST.getId(), ""));
        final List<Task> tasks = service.findAllByProjectId(userId, USER_PROJECT1.getId());
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
        tasks.forEach(task -> Assert.assertEquals(USER_PROJECT1.getId(), task.getProjectId()));
    }

}
